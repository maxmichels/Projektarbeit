package control;

import java.io.File;
import java.io.PrintStream;
import java.util.Map;

import javazoom.jlgui.basicplayer.BasicController;
import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerEvent;
import javazoom.jlgui.basicplayer.BasicPlayerException;
import javazoom.jlgui.basicplayer.BasicPlayerListener;
import library.Playlist;
import project.model.PlayerController;

public class Play implements PlayerController, BasicPlayerListener {
	
	private PrintStream out;
	private BasicPlayer player;
	private BasicController control;
	private Playlist playlist;
	private int songNumber;
	
	/**
	 * Create PlayerControl
	 */
	public Play(){
		out = System.out;
		BasicPlayer player = new BasicPlayer();
		player.addBasicPlayerListener(this);
		this.player = player;
		BasicController control = (BasicController) player;
		this.control = control;
	}

	/**
	 * Start to play a Song
	 * @param path Path to songfile
	 */
	@Override
	public void play(String path) {
		try
		{			
			// Stop playing if playing
			if(player.getStatus()==BasicPlayer.PLAYING) control.stop();
			control.open(new File(path));
			control.play();
		}
		catch (BasicPlayerException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Start to play the playlist with first song.
	 */
	@Override
	public void startPlaylist() {	
		int songNumber = 0;
		play(playlist.trackList.get(songNumber).getFile().toString());
	}

	/**
	 * Stops playing the Playlist.
	 */
	@Override
	public void stopPlaylist(){
		try {
			control.stop();
		} catch (BasicPlayerException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Set the local playlist.
	 * @param playlist New Playlist as Playlist
	 */
	public void setPlaylist(Playlist playlist){
		this.playlist = playlist;
	}

	/**
	 * Open callback, stream is ready to play.
	 *
	 * properties map includes audio format dependant features such as
	 * bitrate, duration, frequency, channels, number of frames, vbr flag,
	 * id3v2/id3v1 (for MP3 only), comments (for Ogg Vorbis), ... 
	 *
	 * @param stream could be File, URL or InputStream
	 * @param properties audio stream properties.
	 * @author http://www.javazoom.net
	 */
	@Override
	public void opened(Object stream, @SuppressWarnings("rawtypes") Map properties) {
		// Pay attention to properties. It's useful to get duration, 
		// bitrate, channels, even tag such as ID3v2.
		display("opened : "+properties.toString());		
		
	}

	/**
	 * Progress callback while playing.
	 * 
	 * This method is called severals time per seconds while playing.
	 * properties map includes audio format features such as
	 * instant bitrate, microseconds position, current frame number, ... 
	 * 
	 * @param bytesread from encoded stream.
	 * @param microseconds elapsed (<b>reseted after a seek !</b>).
	 * @param pcmdata PCM samples.
	 * @param properties audio stream parameters.
	 * @author http://www.javazoom.net
	 */
	@Override
	public void progress(int bytesread, long microseconds, byte[] pcmdata, @SuppressWarnings("rawtypes") Map properties) {
		// Pay attention to properties. It depends on underlying JavaSound SPI
		// MP3SPI provides mp3.equalizer.
		display("progress : "+properties.toString());
		
	}

	/**
	 * A handle to the BasicPlayer, plugins may control the player through
	 * the controller (play, stop, ...)
	 * @param controller : a handle to the player
	 * @author http://www.javazoom.net
	 */	
	@Override
	public void setController(BasicController controller) {
		display("setController : " + controller);
	}
		
	

	/**
	 * Notification for BasicPlayer state
	 * @param event Event input
	 * @author http://www.javazoom.net
	 */
	@Override
	public void stateUpdated(BasicPlayerEvent event) {
		// Notification of BasicPlayer states (opened, playing, end of media, ...)
		display("stateUpdated : "+event.toString());
		if (event.getCode()==BasicPlayerEvent.EOM)
		{
			//TODO after End of Media
			//NEED TO BE REDESIGNED
			//EOM Event wenn Song zuende --> N�chster Song aus Playlist
			songNumber++;
			System.out.println("playing:" + songNumber);
			play(playlist.trackList.get(songNumber).getFile().toString());
		}
		
	}
	
	/**
	 * Print out the Message @param msg on the Console.
	 * @param msg Message to print on console
	 * @author http://www.javazoom.net
	 */
	public void display(String msg)
	{
		if (out != null) out.println(msg);
	}
	
	/**
	 * Set the Volume.
	 * @param gain Input between 1 and 100.
	 */
	public void setGain(int gain){
		if(gain < 0 || gain > 100)return;
		double newGain = (double) gain/100;
		try {
			control.setGain(newGain);
		} catch (BasicPlayerException e) {
			e.printStackTrace();
		}
	}
}
