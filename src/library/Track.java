package library;

/**
 * @author Max Michels
 * @version 1.0
 */
public class Track implements project.model.Track {

	//Attribute
	private String title;
	private String artist;
	private String file;
	
	
	public Track(String title, String artist, String file) {
		this.title = title;
		this.artist = artist;
		this.file = file;
	}
	
	public Track(project.model.Track newTrack) {
		this.title = newTrack.getTitle();
		this.artist = newTrack.getArtist();
		this.file = newTrack.getFile();
	}

	@Override
	public String getArtist() {
		return this.artist;
	}

	@Override
	public String getFile() {
		return this.file;
	}

	@Override
	public String getTitle() {
		return this.title;
	}

	@Override
	public void setArtist(String artist) {
		this.artist = artist;
	}

	@Override
	public void setFile(String file) {
		this.file = file;
	}

	@Override
	public void setTitle(String title) {
		this.title = title;
	}

}
