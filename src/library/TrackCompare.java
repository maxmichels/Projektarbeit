package library;

import java.util.Comparator;
import project.model.Track;

/**
 * Compares the tracks by Artist
 * @author Max Michels
 * @version 1.0
 *
 */

public class TrackCompare implements Comparator<Track>{

	@Override
	public int compare(Track firstTrack, Track secondTrack) {
		//If same artist then compare Title
		if(firstTrack.getArtist() == secondTrack.getArtist()) {
			return firstTrack.getTitle().compareTo(secondTrack.getTitle());
		}else{
			return firstTrack.getArtist().compareTo(secondTrack.getArtist());
		}
	}

}
