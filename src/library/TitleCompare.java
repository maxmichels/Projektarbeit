package library;

import java.util.Comparator;
import project.model.Track;
/**
 * Compares the tracks by title
 * @author Max Michels
 * @version 1.0
 */

public class TitleCompare implements Comparator<Track>{

	@Override
	public int compare(Track firstTrack, Track secondTrack) {
		//If same Title compare artist
		if(firstTrack.getTitle() == secondTrack.getTitle()) {
			return firstTrack.getArtist().compareTo(secondTrack.getArtist());
		}else{
			return firstTrack.getTitle().compareTo(secondTrack.getTitle());
		}
	}

}
