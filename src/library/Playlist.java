package library;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import project.model.PlayList;
import project.model.TrackEntryIncompleteException;
import project.model.TrackFileMissingException;


/**
 * @author Max Michels
 * @version 1.0
 *
 */

public class Playlist implements PlayList {
	
	public LinkedList<project.model.Track> trackList = new LinkedList<>();
	
	@Override
	public void addTrack(project.model.Track track) throws TrackEntryIncompleteException, TrackFileMissingException{
		// Add track to Playlist if track is type project.model.Track and is Complete and file exists
		if(track.getTitle().isEmpty()) throw new TrackEntryIncompleteException(track);
		if(track.getArtist().isEmpty()) throw new TrackEntryIncompleteException(track);
		if(track.getFile().isEmpty()) throw new TrackFileMissingException(track);
		File file = new File(track.getFile());
		if(!file.exists()) throw new TrackFileMissingException(track);
		trackList.add(track);

	}

	@Override
	public List<project.model.Track> getTracks() {
		return trackList;
	}
	
	/**
	 * Opens CSV-File and read it into the playlist list.
	 * Also it is checked if the File is existing and accessible and if the Entry is complete.
	 * @param file File with seperated Values
	 * @param append if true than Playlist will be append to current Playlist
	 * @throws IOException Error accessing File.
	 * @throws TrackEntryIncompleteException Error Track entry is not complete.
	 * @throws TrackFileMissingException Error File for Track missing.
	 */
	public void load(File file, boolean append)
			throws IOException, TrackEntryIncompleteException, TrackFileMissingException {
		if(file==null)return;
		if(!file.exists())return;
		if(!append) this.trackList.clear();
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		String line;
		while((line = reader.readLine()) != null){
			//Split CSV-File
			String[] parts = line.split(";");
			String title = parts[0];
			String interpret = parts[1];
			File path = new File(parts[2]);
			//Create track and add to trackList after checking for file existing
			Track track = new Track(title, interpret, path.toString());
			if(path.exists()){
				this.trackList.add(track);
			}else{
				//If File is missing --> throw Exception
				reader.close();
				throw new TrackFileMissingException(track);
			}
		}
		//Close BufferedReader
		reader.close();
	}

	/**
	 * Save the file seperated by ";" to the file from input
	 * @param file File is the file to save the Playlist
	 */
	@Override
	public void save(File file) throws IOException {
		if(file == null) return;
		try{
		//Save the Playlist as CSV
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		for(project.model.Track track : trackList){
			writer.write(track.getTitle() + ";" + track.getArtist() + ";" + track.getFile());
			writer.newLine();
		}
		//Close BufferedWriter
		writer.close();
		}catch(IOException e){
			throw new IOException(e);
		}

	}

	/**
	 * Sort the Playlist.
	 * @param order Set the Order Value
	 */
	@Override
	public void sortBy(int order) {
		if (order == SORT_BY_ARTIST){
			Collections.sort(trackList, new TrackCompare());
		}
		if(order == SORT_BY_TITLE){
			Collections.sort(trackList, new TitleCompare());
		}
	}
}
