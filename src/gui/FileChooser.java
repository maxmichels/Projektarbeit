package gui;

import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.io.File;
import javax.swing.JFileChooser;

/**
 * Class implements the JFileChooser as needed for the Main Frame.
 * @author Max Michels
 * @version 1.0
 *
 */
public class FileChooser {

	/**
	 * FileChooser returns the Path to the File as String
	 * @param frame Parent Frame
	 * @param filter Filter = 1 = CSV, Filter = 2 = MP3
	 * @return file Path to file as string
	 */
	public static String choose(JFrame frame, int filter){
		JFileChooser fileChooser = new JFileChooser();
		FileNameExtensionFilter fileFilter = null;
		if(filter==1){
			fileFilter = new FileNameExtensionFilter("CSV-Files (*.csv)", "csv");
		}
		if(filter==2){
			fileFilter = new FileNameExtensionFilter("MP3-Files (*.mp3)", "mp3");
		}
        // add filters
		fileChooser.addChoosableFileFilter(fileFilter);
		fileChooser.setFileFilter(fileFilter);
		int path = fileChooser.showOpenDialog(frame);
		if(path == JFileChooser.APPROVE_OPTION){
			File file = fileChooser.getSelectedFile();
			return file.toString();
		}
		return null;
	}
	
	/**
	 * FileChooser returns the Fileobject
	 * @param frame Parent Frame
	 * @param filter Filter = 1 = CSV, Filter = 2 = MP3
	 * @param save If yes the aprove button is set to save
	 * @return file file object
	 */
	public static File chooseFile(JFrame frame, int filter, boolean save){
		JFileChooser fileChooser = new JFileChooser();
		if(save) fileChooser.setApproveButtonText("Speichern");
		FileNameExtensionFilter fileFilter = null;
		String extension = null;
		if(filter==1){
			fileFilter = new FileNameExtensionFilter("CSV-Files (*.csv)", "csv");
			extension = ".csv";
		}
		if(filter==2){
			fileFilter = new FileNameExtensionFilter("MP3-Files (*.mp3)", "mp3");
		}
        // add filters
		fileChooser.addChoosableFileFilter(fileFilter);
		fileChooser.setFileFilter(fileFilter);
		int path = fileChooser.showOpenDialog(frame);
		if(path == JFileChooser.APPROVE_OPTION){
			if(save){
				File file = new File(fileChooser.getSelectedFile()+extension);				
				return file;
			}else{
				File file = fileChooser.getSelectedFile();				
				return file;
			}
		}
		return null;
	
	}
	
	/**
	 * FileChooser for AddSong returns path to file as String
	 * @param addsong Parent Frame
	 * @return file Path to file as string
	 */
	public static String choose(AddSong addsong){
		JFileChooser fileChooser = new JFileChooser();
		FileNameExtensionFilter fileFilter = new FileNameExtensionFilter("MP3-Files (*.mp3)", "mp3");
        // add filters
		fileChooser.addChoosableFileFilter(fileFilter);
		fileChooser.setFileFilter(fileFilter);
		int path = fileChooser.showOpenDialog(addsong);
		if(path == JFileChooser.APPROVE_OPTION){
			File file = fileChooser.getSelectedFile();		
			return file.toString();
		}
		return null;
	}
}
