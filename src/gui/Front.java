package gui;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import control.Play;
import library.Playlist;
import project.model.PlayList;
import project.model.Track;
import project.model.TrackEntryIncompleteException;
import project.model.TrackFileMissingException;

/**
 * Class to implement the GUI main Frame to control the MP3-Player.
 * @author Max Michels
 * @version 1.0
 *
 */
public class Front {

	private JFrame frame;
	private Playlist pl;
	private Play player = new Play();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Front window = new Front();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Front() {
		pl = new Playlist();
		initialize();
	}
	
	/**
	 * Method initialize the main frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(Playlist.class.getResource("/gui/mp3_img.png")));
		frame.setTitle("MP3 Player");
		frame.setBounds(100, 100, 450, 150);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 434, 24);
		frame.setJMenuBar(menuBar);
		
		JMenu mnDatei = new JMenu("Datei");
		menuBar.add(mnDatei);
		
		JMenuItem mntmLoadPlaylist = new JMenuItem("Playlist laden");
		mntmLoadPlaylist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					loadPlaylist(false);
				} catch (Exception e) {
					// If error loading playlist
					e.printStackTrace();
					JOptionPane.showMessageDialog(frame,
							"Die Datei konnte nicht geladen werden. Bitte versuchen Sie es erneut.",
							"Warnung",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		mnDatei.add(mntmLoadPlaylist);
		
		JMenuItem mntmAddEntry = new JMenuItem("Eintrag hinzuf\u00FCgen");
		mntmAddEntry.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddSong as = new AddSong(frame);
				try {
					pl.addTrack(as.getTrack());
					as.dispose();
					loadTrackLayout();
				} catch (TrackEntryIncompleteException | TrackFileMissingException e) {
					e.printStackTrace();
				}
			}
		});
		
		JMenuItem mntmLoadAndAppend = new JMenuItem("Neue Playlist laden und anh\u00E4ngen");
		mntmLoadAndAppend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					loadPlaylist(true);
				} catch (Exception e) {
					// If error loading playlist
					e.printStackTrace();
					JOptionPane.showMessageDialog(frame,
							"Die Datei konnte nicht geladen werden. Bitte versuchen Sie es erneut.",
							"Warnung",
							JOptionPane.WARNING_MESSAGE);
				}
			}
		});
		mnDatei.add(mntmLoadAndAppend);
		
		JMenuItem mntmSavePlaylist = new JMenuItem("Playlist speichern");
		mntmSavePlaylist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					try {
						//If tracklist is empty abort method
						if(pl.trackList.isEmpty()){
							JOptionPane.showMessageDialog(frame,
									"Die Playlist beinhaltet keine Titel.",
									"Warnung",
									JOptionPane.WARNING_MESSAGE);
							return;
						}
						save();
					} catch (IOException e) {
						// If error saving playlist
						e.printStackTrace();
						JOptionPane.showMessageDialog(frame,
								"Die Playlist konnte nicht gespeichert werden.\nBitte versuchen Sie es erneut.",
								"Warnung",
								JOptionPane.WARNING_MESSAGE);
					}
				
			}
		});
		mnDatei.add(mntmSavePlaylist);
		mnDatei.add(mntmAddEntry);
		
		JMenuItem mntmClose = new JMenuItem("Beenden");
		mntmClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		mnDatei.add(mntmClose);
		
		JMenu mnSortieren = new JMenu("Sortieren");
		menuBar.add(mnSortieren);
		
		JMenuItem mntmSortInterpret = new JMenuItem("Sortieren nach Interpret");
		mntmSortInterpret.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pl.sortBy(PlayList.SORT_BY_ARTIST);
				loadTrackLayout();
			}
		});
		mnSortieren.add(mntmSortInterpret);
		
		JMenuItem mntmSortTitle = new JMenuItem("Sortieren nach Titel");
		mntmSortTitle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pl.sortBy(PlayList.SORT_BY_TITLE);
				loadTrackLayout();
			}
		});
		mnSortieren.add(mntmSortTitle);
		
		//Adds Menuitem "�ber" to open About frame
		JMenuItem mntmber = new JMenuItem("\u00DCber");
		mntmber.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			About.main();
			}
		});
		menuBar.add(mntmber);
		
		loadTrackLayout();
	}

	/**
	 * Disposes the main Frame and terminates all other instances created by this application.
	 */
	private void close(){
		frame.dispose();
		System.exit(0);
	}

	/**
	 * Load Playlist from file and appends or loads instead
	 * @param append True if playlist append to current
	 * @throws Exception 
	 * @throws TrackEntryIncompleteException 
	 * @throws IOException 
	 */
	private void loadPlaylist(Boolean append) throws IOException, TrackEntryIncompleteException, Exception{
		pl.load((FileChooser.chooseFile(frame, 1, false)), append);
		loadTrackLayout();
	}
	
	/**
	 * Method to save the playlist as CSV.
	 * @throws IOException
	 */
	private void save() throws IOException{
		try {
			pl.save(FileChooser.chooseFile(frame, 1, true));
		} catch (IOException e) {
			throw new IOException(e);
		}
	}
	
	/**
	 * If the trackList is empty the Method will exit.
	 * Loading Button and title for each song in trackList.
	 * If the "Play all" Button is not created he will be created.
	 */
	private void loadTrackLayout(){
		frame.getContentPane().removeAll();
		if(pl.trackList.isEmpty()){
			JLabel lblNoTracks = new JLabel("Es ist noch keine Playlist ge\u00F6ffnet.");
			lblNoTracks.setBounds(10, 10, 208, 21);
			frame.getContentPane().add(lblNoTracks);
			return;
		}
		int i = 0;
		//Get Play-Button and Tracktitle for each track in tracklist
		frame.setBounds(100, 100, 450, pl.trackList.size()*40+150);
		for(@SuppressWarnings("unused") Track track : pl.trackList){	
			String path = pl.trackList.get(i).getFile();
			JButton button = new JButton("Button" + i);
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					player.play(path);
				}
			});
			button.setToolTipText("Play");
			button.setIcon(new ImageIcon(Playlist.class.getResource("/gui/play_button_2.png")));
			button.setBounds(10, (30*(i+1)+(i*10)+35), 30, 30);
			frame.getContentPane().add(button);
			button.setVisible(true);
			
			JLabel lblTracktitle = new JLabel(pl.trackList.get(i).getTitle().toString());
			lblTracktitle.setBounds(50, (30*(i+1)+(i*10)+35), 374, 30);
			lblTracktitle.setToolTipText("Interpret: " + pl.trackList.get(i).getArtist());
			frame.getContentPane().add(lblTracktitle);
			
			i++;
		}
		
		//Enabling Play All Button
		if(!pl.trackList.isEmpty()){
			JButton btnPlayAllSongs = new JButton("Play All Songs");
			btnPlayAllSongs.setToolTipText("Spielt alle Title der Playlist ab");
			btnPlayAllSongs.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					player.setPlaylist(pl);
					player.startPlaylist();
				}
			});
			btnPlayAllSongs.setBounds(10, 0, 414, 20);
			frame.getContentPane().add(btnPlayAllSongs);
		}
		
		//Adding Volume slider
		if(!pl.trackList.isEmpty()){
			JSlider slider = new JSlider();
			slider.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent arg0) {
					player.setGain(slider.getValue());
				}
			});
			slider.setValue(80);
			slider.setToolTipText("Volume");
			slider.setBounds(80, 35, 320, 30);
			frame.getContentPane().add(slider);
			
			JLabel lblVolume = new JLabel("Volume");
			lblVolume.setBounds(10, 35, 50, 30);
			frame.getContentPane().add(lblVolume);
		}
		frame.repaint();
	}
	
	/**
	 * Returns the playlist stored in the frame.
	 * @return pl Playlist as project.model.playlist
	 */
	public Playlist getPlaylist(){
		return pl;
	}
	
	/**
	 * Set the local playlist stored in the frame.
	 * @param playlist playlist as Playlist
	 */
	protected void setPlaylist(Playlist playlist){
		this.pl = playlist;
	}
}
