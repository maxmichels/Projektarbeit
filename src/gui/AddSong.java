package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import library.Track;
import project.model.TrackEntryIncompleteException;
import project.model.TrackFileMissingException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

/**
 * Class implements a JDialog to create a new Track. The Track can then added to the trackList in the main Frame.
 * @author Max Michels
 * @version 1.0
 *
 */
@SuppressWarnings("serial")
public class AddSong extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField tfTitle;
	private JTextField tfInterpret;
	private JTextField tfPath;
	private Track track;

	/**
	 * Create the dialog frame.
	 * @param frame Parent Frame.
	 */
	public AddSong(JFrame frame) {
		super(frame,true);
		
		setBounds(100, 100, 450, 205);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel label = new JLabel("Titel:");
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setBounds(29, 14, 46, 14);
		contentPanel.add(label);
		
		tfTitle = new JTextField();
		tfTitle.setColumns(10);
		tfTitle.setBounds(85, 11, 203, 20);
		contentPanel.add(tfTitle);
		
		JLabel label_1 = new JLabel("Interpret:");
		label_1.setHorizontalAlignment(SwingConstants.RIGHT);
		label_1.setBounds(10, 39, 65, 14);
		contentPanel.add(label_1);
		
		tfInterpret = new JTextField();
		tfInterpret.setColumns(10);
		tfInterpret.setBounds(85, 36, 203, 20);
		contentPanel.add(tfInterpret);
		
		JLabel label_2 = new JLabel("Dateipfad");
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);
		label_2.setBounds(10, 67, 65, 14);
		contentPanel.add(label_2);
		
		tfPath = new JTextField();
		tfPath.setEditable(false);
		tfPath.setColumns(10);
		tfPath.setBounds(85, 64, 203, 20);
		contentPanel.add(tfPath);
		
		JButton btnChooseFile = new JButton("Datei ausw\u00E4hlen");
		btnChooseFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				fileChoose();
			}
		});
		btnChooseFile.setBounds(85, 95, 203, 23);
		contentPanel.add(btnChooseFile);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							addTrack();
						} catch (TrackEntryIncompleteException e1) {
							// Error Message if Exception
							e1.printStackTrace();
							JFrame frame = new JFrame();
							JOptionPane.showMessageDialog(frame,
									"Bitte f�llen Sie alle Felder aus.",
									"Warnung",
									JOptionPane.WARNING_MESSAGE);

						} catch (TrackFileMissingException e1) {
							// Error Message if File is missing or is not existing 
							e1.printStackTrace();
							JFrame frame = new JFrame();
							JOptionPane.showMessageDialog(frame,
									"Bitte w�hlen Sie eine Datei aus.",
									"Warnung",
									JOptionPane.WARNING_MESSAGE);
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						close();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}
	
	/**
	 * Opens new Frame to select File and print the Path into the text field.
	 */
	private void fileChoose(){
		tfPath.setText(FileChooser.choose(this));
	}
	
	/**
	 * Set the visibility false. Can not be disposed because the main frame need to import the track first to the trackList.
	 */
	private void close(){
		this.setVisible(false);
	}
	
	/**
	 * Create an track with the information from the JDialog frame.
	 * @throws TrackEntryIncompleteException Error Track entry is not complete.
	 * @throws TrackFileMissingException Error File for Track missing.
	 */
	public void addTrack() throws TrackEntryIncompleteException, TrackFileMissingException{
		String title;
		String interpret;
		String path;
		if(this.tfTitle.getText().isEmpty()){
			throw new TrackEntryIncompleteException(this.tfTitle.getText());
		}else{
			title = this.tfTitle.getText();
		}
		
		if(this.tfInterpret.getText().isEmpty()){
			throw new TrackEntryIncompleteException(this.tfInterpret.getText());
		}else{
			interpret = this.tfInterpret.getText();
		}
		
		path = this.tfPath.getText();
		File file = new File(path);
		this.track = new Track(title, interpret, file.toString());
		
		close();
	}
	
	/**
	 * Returns the created track then disposes this JDialog frame.
	 * @return track Local created track as Track.
	 */
	public Track getTrack(){
		return this.track;
	}
}
