package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

/**
 * Class creates a frame with information about this application.
 * @author Max Michels
 * @version 1.0 
 *
 */

@SuppressWarnings("serial")
public class About extends JFrame {

	private JPanel contentPane;
	private static JFrame jFrame;

	/**
	 * Launch the Frame.
	 */
	public static void main() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					About frame = new About();
					frame.setVisible(true);
					jFrame = frame;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public About() {
		setAlwaysOnTop(true);
		setIconImage(Toolkit.getDefaultToolkit().getImage(About.class.getResource("/gui/mp3_img.png")));
		setResizable(false);
		setTitle("About");
		setBounds(100, 100, 451, 227);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextPane txtpnAbout = new JTextPane();
		txtpnAbout.setBackground(SystemColor.control);
		txtpnAbout.setEditable(false);
		txtpnAbout.setText("Dieses Java-Projekt wurde von Max Michels im Rahmen des Moduls \"Grundlagen der Informatik und Programmiersprachen\" geschrieben.\r\nEs soll einen MP3-Player implementieren der Playlists laden und speichern kann.\r\n\r\nUm das abspielen der MP3-Dateien zu erm\u00F6glichen wurde die API BasicPlayer <http://www.javazoom.net/jlgui/api.html> benutzt.");
		txtpnAbout.setBounds(10, 11, 414, 132);
		contentPane.add(txtpnAbout);
		
		JButton btnSchliessen = new JButton("Schlie\u00DFen");
		btnSchliessen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				jFrame.dispose();
			}
		});
		btnSchliessen.setBounds(314, 154, 110, 23);
		contentPane.add(btnSchliessen);
	}
}
